package catalog;

import javax.persistence.Embeddable;

@Embeddable
public class CategoryBoundary {

	private String name;
	private String description;

	public CategoryBoundary() {
	}
	
	public CategoryBoundary(Category category) {
		this.name = category.getName();
		this.description = category.getDescription();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CategoryBoundary [name=" + name + ", description=" + description + "]";
	}

	

}
