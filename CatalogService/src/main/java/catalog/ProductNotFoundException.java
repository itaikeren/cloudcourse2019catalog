package catalog;

public class ProductNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 4115912177382448022L;

	public ProductNotFoundException() {
	}

	public ProductNotFoundException(String message) {
		super(message);
	}

	public ProductNotFoundException(Throwable cause) {
		super(cause);
	}

	public ProductNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
