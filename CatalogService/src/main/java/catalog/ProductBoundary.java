package catalog;

import io.swagger.annotations.ApiModelProperty;

public class ProductBoundary {

	@ApiModelProperty(required = false, hidden = true)
	private String id;
	private String name;
	private float price;
	private String image;
	private CategoryBoundary category;

	public ProductBoundary() {
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public CategoryBoundary getCategory() {
		return category;
	}

	public void setCategory(CategoryBoundary category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return "ProductBoundary [id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + ", category="
				+ category + "]";
	}

}
