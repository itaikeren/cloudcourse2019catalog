package catalog;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ProductCrud extends PagingAndSortingRepository<Product, String>{
	                                 
	public List<Product> findByNameLike(@Param("PRODUCTNAME") String name, Pageable pageable);
	public List<Product> findByPriceGreaterThanEqual(@Param("price") float minPrice, Pageable pageable);
	public List<Product> findByPriceLessThanEqual(@Param("price") float maxPrice, Pageable pageable);
	public List<Product> findByCategoryNameLike(@Param("category") String name, Pageable pageable);
	


}
