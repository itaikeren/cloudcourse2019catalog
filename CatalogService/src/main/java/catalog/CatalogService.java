package catalog;

import java.util.List;
import java.util.Optional;

public interface CatalogService {
	public Category create (Category category);
	public Product create (Product product);
	public Optional<Product> getProductById (String id);
	public List<Product> getAllProduct(int page, int size, String sort);
	public List<Product> getProductByName(String name, int page, int size, String sort);
	public List<Product> getProductbyMinPrice(float minPrice, int page, int size, String sort);
	public List<Product> getProductByMaxPrice(float maxPrice, int page, int size, String sort);
	public List<Product> getProductByCategoryName(String name, int page, int size, String sort);
	public List<Category> getAllCategories(int page, int size, String sort);
	public void deleteAll();
}
