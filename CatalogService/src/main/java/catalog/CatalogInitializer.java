package catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("init")
public class CatalogInitializer implements CommandLineRunner {
	private CatalogService catalog;

	@Autowired
	public CatalogInitializer(CatalogService catalog) {
		super();
		this.catalog = catalog;
	}

	@Override
	public void run(String... args) throws Exception {
		CategoryBoundary computers = new CategoryBoundary(this.catalog.create(new Category("computers", "personal and business computers and peripherals")));
		CategoryBoundary apparels = new CategoryBoundary(this.catalog.create(new Category("apparels", "fashionable products")));

		this.catalog.create(new Product("", "laptop1", 100, "img1",computers));
		this.catalog.create(new Product("", "laptop2", 30, "img1",computers));
		this.catalog.create(new Product("", "laptop3", 550, "img1",computers));

		this.catalog.create(new Product("", "shirt1", 200, "img1",apparels));
		this.catalog.create(new Product("", "shirt2", 400, "img1",apparels));
	}

}
