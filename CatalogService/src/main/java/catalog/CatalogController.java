package catalog;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CatalogController {
	private CatalogService catalog;

	@Autowired
	public CatalogController(CatalogService catalog) {
		super();
		this.catalog = catalog;
	}

	@RequestMapping(path = "/catalog/categories",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public CategoryBoundary create(@RequestBody CategoryBoundary category) {
		
		return fromEntity(
				this.catalog
				.create(
						toEntity(category)
						)
				);
	}

	@RequestMapping(path = "/catalog/products",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductBoundary create(@RequestBody ProductBoundary product) {
		return fromEntity(
				this.catalog
				.create(
						toEntity(product)
						)
				);
	}

	@RequestMapping(path = "/catalog/products/{productId}",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductBoundary getProductById(
			@PathVariable("productId") String productId) {
		
		return this.fromEntity(
				this.catalog
					.getProductById(productId)
					.orElseThrow(() -> new ProductNotFoundException("could not find product by id: " + productId)));		
	}

	@RequestMapping(path = "/catalog/categories",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public CategoryBoundary[] getAllCategories(
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
			@RequestParam(name = "size", required = false, defaultValue = "20") int size,
			@RequestParam(name = "sortBy", required = false, defaultValue = "name") String sort) {
		return this.catalog
				.getAllCategories(page, size, sort)
				.stream()
				.map(this::fromEntity)
				.collect(Collectors.toList())
				.toArray(new CategoryBoundary[0]);
	}

	@RequestMapping(path = "/catalog/products",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductBoundary[] getAllProducts(
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
			@RequestParam(name = "size", required = false, defaultValue = "20") int size,
			@RequestParam(name = "sortBy", required = false, defaultValue = "id") String sort,
			@RequestParam(name = "filterType", required = false, defaultValue = "") String filterType,
			@RequestParam(name = "filterValue", required = false) String filterValue) {
		switch (filterType) {
		
		case "byname":
		case "byName":
			if(!filterValue.isEmpty()) {
				return this.catalog
						.getProductByName(filterValue, page, size, sort)
						.stream()
						.map(this::fromEntity)
						.collect(Collectors.toList())
						.toArray(new ProductBoundary[0]);		
			} else {
				throw new IllegalArgumentException("Please enter the filter value!");
			}
		case "byminprice":
		case "byMinPrice":
			if(!filterValue.isEmpty()) {
				return this.catalog
						.getProductbyMinPrice(Float.parseFloat(filterValue), page, size, sort)
						.stream()
						.map(this::fromEntity)
						.collect(Collectors.toList())
						.toArray(new ProductBoundary[0]);		
			} else {
				throw new IllegalArgumentException("Please enter the filter value!");
			}
		case "bymaxprice":
		case "byMaxPrice":
			if(!filterValue.isEmpty()) {
				return this.catalog
						.getProductByMaxPrice(Float.parseFloat(filterValue), page, size, sort)
						.stream()
						.map(this::fromEntity)
						.collect(Collectors.toList())
						.toArray(new ProductBoundary[0]);		
			} else {
				throw new IllegalArgumentException("Please enter the filter value!");
			}
		case "bycategoryname":
		case "byCategoryName":
			if(!filterValue.isEmpty()) {
				return this.catalog
						.getProductByCategoryName(filterValue, page, size, sort)
						.stream()
						.map(this::fromEntity)
						.collect(Collectors.toList())
						.toArray(new ProductBoundary[0]);		
			} else {
				throw new IllegalArgumentException("Please enter the filter value!");
			}
		default:
			return this.catalog
					.getAllProduct(page, size, sort)
					.stream()
					.map(this::fromEntity)
					.collect(Collectors.toList())
					.toArray(new ProductBoundary[0]);
		}
	}
	
	@RequestMapping(
			path="/catalog", 
			method = RequestMethod.DELETE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteAll() {
		this.catalog.deleteAll();
	}
	
	private ProductBoundary fromEntity (Product product) {
		ProductBoundary rv = new ProductBoundary();
		
		rv.setId(product.getId());
		rv.setName(product.getName());
		rv.setPrice(product.getPrice());
		rv.setImage(product.getImage());
		rv.setCategory(product.getCategory());
		
		return rv;
	}
	
	private Product toEntity (ProductBoundary product) {
		try {
			Product rv = new Product();
			
			rv.setName(product.getName());
			rv.setPrice(product.getPrice());
			rv.setImage(product.getImage());
			rv.setCategory(product.getCategory());
			
			return rv;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private CategoryBoundary fromEntity (Category category) {
		CategoryBoundary rv = new CategoryBoundary();
		
		rv.setName(category.getName());
		rv.setDescription(category.getDescription());
		
		return rv;
	}
	
	private Category toEntity (CategoryBoundary category) {
		try {
			Category rv = new Category();
			
			rv.setName(category.getName());
			rv.setDescription(category.getDescription());
			
			return rv;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String, String> handleException (ProductNotFoundException e){
		return Collections.singletonMap("error", "prodcut not found");
	}
	
	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public Map<String, String> handleException (IllegalArgumentException e){
		return Collections.singletonMap("error", e.getMessage());
	}

}
