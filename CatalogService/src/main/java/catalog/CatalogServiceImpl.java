package catalog;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CatalogServiceImpl implements CatalogService {

	private CategoryCrud categories;
	private ProductCrud products;
	
	@Autowired
	public CatalogServiceImpl(CategoryCrud categories, ProductCrud products) {
		super();
		this.categories = categories;
		this.products = products;
	}
	
	@Override
	@Transactional
	public Category create(Category category) {
		String categoryName = category.getName();
		if(this.categories.existsById(categoryName)) {
			throw new RuntimeException("This category already exists!");
		}
		return this.categories.save(category);
	}
	
	@Override
	@Transactional
	public Product create(Product product) {
		String categoryName = product.getCategory().getName();
		if(!this.categories.existsById(categoryName)) {
			throw new RuntimeException("This product category is NOT exists!");
		}
		return this.products.save(product);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Product> getProductById(String id) {
		return this.products.findById(id);
	}

	@Override
	@Transactional
	public List<Product> getAllProduct(int page, int size, String sort) {
		return this.products
				.findAll(PageRequest.of(page, size, Direction.DESC, sort))
				.getContent();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Product> getProductByName(String name, int page, int size, String sort) {
		return this.products
				.findByNameLike('%'+name+'%', PageRequest.of(page, size, Direction.DESC, sort));
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Product> getProductbyMinPrice(float minPrice, int page, int size, String sort) {
		return this.products
				.findByPriceGreaterThanEqual(minPrice, PageRequest.of(page, size, Direction.DESC, sort));
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Product> getProductByMaxPrice(float maxPrice, int page, int size, String sort) {
		return this.products
				.findByPriceLessThanEqual(maxPrice, PageRequest.of(page, size, Direction.DESC, sort));
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Product> getProductByCategoryName(String name, int page, int size, String sort) {
		return this.products
				.findByCategoryNameLike('%'+name+'%', PageRequest.of(page, size, Direction.DESC, sort));
	}
	
	@Override
	@Transactional
	public List<Category> getAllCategories(int page, int size, String sort) {
		return this.categories
				.findAll(PageRequest.of(page, size, Direction.DESC, sort))
				.getContent();
	}

	@Override
	@Transactional
	public void deleteAll() {
		this.products.deleteAll();
		this.categories.deleteAll();

	}

}
